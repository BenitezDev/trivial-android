package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Activity que pregunta el nombre de los jugadores
 *
 * @author Alejandro Benítez en Enero 2019
 */
public class AskNamesActivity extends Activity
{

    public static final String PLAYER_1_NAME = "es.esne.eop.trivial.PLAYER_1_NAME";
    public static final String PLAYER_2_NAME = "es.esne.eop.trivial.PLAYER_2_NAME";


    // Variables donde se escribirán los nombres de los jugadores
    private EditText player_1_name,
                     player_2_name;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_names);

        // Guardar las referencias
        player_1_name = (EditText) findViewById(R.id.player1name);
        player_2_name = (EditText) findViewById(R.id.player2name);

        // Inicializar el botón de jugar
        Button play_button = findViewById(R.id.play_button);

        // Dar funcionalizad al botón
        play_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Comprobamos si los nombres son válidos (más de 3 caracteres por nombre)
                if(player_1_name.getText().length() >= 3 && player_2_name.getText().length() >= 3)
                {
                    Intent intent = new Intent(getBaseContext() , GameActivity.class);

                    // Por no poner el .toString() me llevó como media hora saber porque no funcionaba :)
                    intent.putExtra( PLAYER_1_NAME , player_1_name.getText().toString());
                    intent.putExtra( PLAYER_2_NAME , player_2_name.getText().toString());

                    // Llamamos a la GameActivity
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(AskNamesActivity.this, "Por favor, introduce nombres válidos", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    @Override
    public void finish() {
        super.finish();
        System.exit(0);
    }
}
