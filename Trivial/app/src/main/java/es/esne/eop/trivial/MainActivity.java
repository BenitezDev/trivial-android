package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


/**
 * Primer Activity de bienvenida
 *
 * @author Alejandro Benítez en Enero 2019
 */
public class MainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Guardar las  referencias a los botones
        Button play_button = (Button) findViewById(R.id.play_button);
        Button quit_button = (Button) findViewById(R.id.quit_button);


        // Implementar la pulsación del botón de jugar
        play_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(),AskNamesActivity.class);
                startActivity(intent);
            }
        });

        // Guardar la pulsación del botón de salir
        quit_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
                System.exit(0);
            }
        });


    }

}
