package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

/**
 * Clase principal del juego
 *
 * @author Alejandro Benítez en Enero 2019
 */
public class GameActivity extends Activity
{

    public static final String GANADOR = "es.esne.eop.trivial.GANADOR";

    // Siento tener que hacer esto. Ha sido la forma que he encontrado para solucionar los problemas
    // que he tenido a la hora de pasar información de esta activity a la activity de respuestas.
    private static Context mContext;

    /*
        primera dimension:
            [0] -> primer  jugador
            [1] -> segundo jugador
        Segunda dimension:
            [0] -> AZUL     -> Geografía
            [1] -> PINK     -> Entretenimiento
            [2] -> YELLOW   -> Historia
            [3] -> BROWN    -> Arte y literatura
            [4] -> GREEN    -> Ciencia y naturaleza
            [5] -> ORANGE   -> Deporte y ocio
     */
    // Guarda la representación gráfica de los aciertos de cada jugador
    // A medida que aciertan preguntas van consiguiendo quesitos
    // Solo consigues un quesito por cada categoria la primera vez que aciertas.
    static ImageView[][] quesitos = new ImageView[2][6];

    // Guarda la representación lógica de los aciertos de cada jugador
    // True = pregunta acertada     False = pregunta no acertada
    static boolean[][] preguntas_acertadas_jugadores = new boolean[2][6];


    // Nombre de los jugadores
    static String[] names = new String[2];


    // Gestión
    static int turno_del_juego = 0;     // 0 es jugador 1
                                        // 1 es jugador 2

    // ESTADO DE LOS JUGADORES (JUGANDO O ESPERANDO)
    static  TextView estado_player_1, estado_player_2;




    // Botón de tirar de la ruleta
    private Button botonTirarRuleta;

    // Imagen de la ruleta
    ImageView ruleta;

    // Texto emergente para aclarar que tipo de categoría te ha tocado
    TextView tipo_pregunta;

    // Variables para gestionar la rotación de la ruleta
    int degree = 0, degree_old = 0;

    // Una circunferencia tiene 360 grados. como hay 6 casillas, tocan a 60 grados cada casilla.
    private static final int FACTOR = 60;


    // Por algún motivo no era capaz de mandar en un intent un Int para gestionar la pregunta
    // que ha tocado.
    public static String indice_de_pregunta;

    // Flag que sirve para que no puedas tirar de la ruleta t0do el rato. Además activa
    // que en cualquier parte que toques de la pantalla (una vez tirado de la ruleta)
    // te lleva a la pregunta
    private boolean wait_for_tap = false;


    Intent intent;
    Intent intent_preguntas;
    Random random;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Guardamos el contexto actual (para así poderlo pasar como parametro en una función estática
        mContext = this;

        // Capturamos el intent de la AskNamesActivity
        getMyIntent();

        // Capturamos el intent de QuestionActivity
        intent_preguntas = getIntent();


        preparar_quesitos();

        preparar_jugadores();

        update_player_state();

        preparar_ruleta();

        /**
         * Se ejecuta al pulsar el botón de tirar de la ruleta.
         */
        botonTirarRuleta.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if(!wait_for_tap) {

                    degree_old = degree % 360;
                    degree = random.nextInt(3600) + 720;

                    RotateAnimation rotate = new RotateAnimation(degree_old, degree,
                            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                            RotateAnimation.RELATIVE_TO_SELF, 0.5f);

                    rotate.setDuration(3600);
                    rotate.setFillAfter(true);
                    rotate.setInterpolator(new DecelerateInterpolator());
                    rotate.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            wait_for_tap = false;
                            tipo_pregunta.setText("");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            tipo_pregunta.setText(categoria_garadora(360 - (degree % 360)));
                            wait_for_tap = true;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    ruleta.startAnimation(rotate);
                }
                else{
                    // Si ya has tirado de la ruleta y vuelves a darle al botón de tirar de ella,
                    // Te lleva a la activity de las preguntas. No dejo que vuelva a tirar
                    wait_for_tap = false;

                    // Lanzamos la activity de las preguntas
                    Intent intent_question = new Intent(getBaseContext(), QuestionActivity.class);
                    startActivity(intent_question);
                }

            }
        });

        // Cuando ya has tirado de la ruleta, se activa esto, y hace clikeable toda la pantalla
        // Cuando la tocas, te lleva a la Activity de las preguntas
        LinearLayout lay = (LinearLayout) findViewById(R.id.fondo);

        lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(wait_for_tap)
                {
                    wait_for_tap = false;
                    Intent intent_question = new Intent(getBaseContext(), QuestionActivity.class);
                    // Lanzamos la activity de las preguntas
                    startActivity(intent_question);
                }
            }
        });

    }  // FIN DEL ONCREATE


    /**
     * Inicializa las variables para gestionar la ruleta
     */
    private void preparar_ruleta()
    {
        botonTirarRuleta = (Button)     findViewById(R.id.BotonTirarRuleta);
        ruleta           = (ImageView)  findViewById(R.id.ruletaImagen);
        tipo_pregunta    = (TextView)   findViewById(R.id.TipoPregunta);

        random = new Random();
    }

    /**
     * Calcula la rotación de la ruleta, lo interpreta y devuelve la categoría que ha salido
     * @param degrees Cuanto ha girado la ruleta
     * @return String con el texto emergente aclarando la categoria que te ha tocado
     */
    private String categoria_garadora(int degrees)
    {
        /*
            [0] -> AZUL     -> Geografía
            [1] -> PINK     -> Entretenimiento
            [2] -> YELLOW   -> Historia
            [3] -> BROWN    -> Arte y literatura
            [4] -> GREEN    -> Ciencia y naturaleza
            [5] -> ORANGE   -> Deporte y ocio
         */

        String text = "";
        // entre 60 y 120 grados
        if(degrees >= (FACTOR) && degrees < (FACTOR*2))
        {
            text = "CIENCIA Y NATURALEZA!";
            indice_de_pregunta = "4";
        }
        // entre 120 y 180 grados
        else if(degrees >= (FACTOR*2) && degrees < (FACTOR*3))
        {
            text = "ARTE Y LITERATURA!";
            indice_de_pregunta = "3";
        }

        // entre 180 y 240 grados
        else if(degrees >= (FACTOR*3) && degrees < (FACTOR*4))
        {
            text = "GEOGRAFÍA!";
            indice_de_pregunta = "0";
        }

        // entre 240 y 300 grados
        else if(degrees >= (FACTOR*4) && degrees < (FACTOR*5))
        {
            text = "HISTORIA!";
            indice_de_pregunta = "2";
        }

        // entre 300 y 360 grados
        else if(degrees >= (FACTOR*5) && degrees < (FACTOR*6))
        {
            text = "DEPORTE Y OCIO!";
            indice_de_pregunta = "5";
        }
        else if(degrees >= 0 && degrees < (FACTOR))
        {
            text = "ENTRETENIMIENTO!";
            indice_de_pregunta = "1";
        }

        return text;
    }


    /**
     * Se llama a la funcioón cuando se acierta una pregunta. Dependiendo del índice de la pregunta,
     * cambia le representación gráfica y lógica si procede (si no se ha acertado la misma categoría ya)
     */
    public static void acertar_pregunta()
    {
        // No me gusta nada tener que hacerlo con un string... Pero no he podido hacerlo con un int
        switch (indice_de_pregunta)
        {
            case "0":
                    preguntas_acertadas_jugadores[turno_del_juego][0] = true;
                    quesitos[turno_del_juego][0].setVisibility(View.VISIBLE);
                break;

            case "1":
                preguntas_acertadas_jugadores[turno_del_juego][1] = true;
                quesitos[turno_del_juego][1].setVisibility(View.VISIBLE);
                break;

            case "2":
                preguntas_acertadas_jugadores[turno_del_juego][2] = true;
                quesitos[turno_del_juego][2].setVisibility(View.VISIBLE);
                break;

            case "3":
                preguntas_acertadas_jugadores[turno_del_juego][3] = true;
                quesitos[turno_del_juego][3].setVisibility(View.VISIBLE);
                break;

            case "4":
                preguntas_acertadas_jugadores[turno_del_juego][4] = true;
                quesitos[turno_del_juego][4].setVisibility(View.VISIBLE);
                break;
            case "5":preguntas_acertadas_jugadores[turno_del_juego][5] = true;
                quesitos[turno_del_juego][5].setVisibility(View.VISIBLE);
                break;

        }
        // Comprobamos si hay ganador
        GameActivity.comprobar_ganador();

    }


    /**
     * Se llama cuando un jugador falla la pregunta. Cambia el jugador y los estados
     */
    static void fallar_pregunta()
    {
        if(turno_del_juego == 0) turno_del_juego = 1;
        else turno_del_juego = 0;

        update_player_state();
    }

    /**
     * Prepara la representación gráfica y lógica de los quesitos
     * Hace invisibles los quesitos e inicializa preguntas_acertadas_jugadores[][] a false
     */
    private void preparar_quesitos()
    {
        /*
        primera dimension:
            [0] -> primer  jugador
            [1] -> segundo jugador
        Segunda dimension:
            [0] -> AZUL     -> Geografía
            [1] -> PINK     -> Entretenimiento
            [2] -> YELLOW   -> Historia
            [3] -> BROWN    -> Arte y literatura
            [4] -> GREEN    -> Ciencia y naturaleza
            [5] -> ORANGE   -> Deporte y ocio
     */

        // Primer jugador
        quesitos[0][0] = findViewById(R.id.quesito_azul_player_1    );
        quesitos[0][1] = findViewById(R.id.quesito_rosa_player_1    );
        quesitos[0][2] = findViewById(R.id.quesito_amarillo_player_1);
        quesitos[0][3] = findViewById(R.id.quesito_marron_player_1  );
        quesitos[0][4] = findViewById(R.id.quesito_verde_player_1   );
        quesitos[0][5] = findViewById(R.id.quesito_naranja_player_1 );
        // Segundo jugador
        quesitos[1][0] = findViewById(R.id.quesito_azul_player_2    );
        quesitos[1][1] = findViewById(R.id.quesito_rosa_player_2    );
        quesitos[1][2] = findViewById(R.id.quesito_amarillo_player_2);
        quesitos[1][3] = findViewById(R.id.quesito_marron_player_2  );
        quesitos[1][4] = findViewById(R.id.quesito_verde_player_2   );
        quesitos[1][5] = findViewById(R.id.quesito_naranja_player_2 );

        // no habrá más de dos jugadores, asi que lo hardcodeo, estoy loquísimo
        for( int i = 0 ; i < 2 ; ++i)
        {
            // Vuelvo a estar puto loco, no va a haber más de 6 categorías
            for ( int j = 0; j < 6; ++j)
            {
                // Pone invisible todos los quesitos
                quesitos[i][j].setVisibility(View.INVISIBLE);
                // Pone false (pregunta no acertada) a todas las preguntas de los jugadores
                preguntas_acertadas_jugadores[i][j] = false;
            }
        }

    }

    /**
     * Comprueba si algún jugador ha ganado
     */
    public static void comprobar_ganador()
    {
        boolean endgame = false;

        for(int i = 0; i < 2 && !endgame ; ++i)
        {
            // Sí, es un poco cutre... Pero funciona
            if(preguntas_acertadas_jugadores[i][0] && preguntas_acertadas_jugadores[i][1] &&
               preguntas_acertadas_jugadores[i][2] && preguntas_acertadas_jugadores[i][3] &&
               preguntas_acertadas_jugadores[i][4] && preguntas_acertadas_jugadores[i][5]   )
            {
                endgame = true;
                Intent end_intent = new Intent(mContext, WinScreenActivity.class);
                end_intent.putExtra(GANADOR, names[i]);
                mContext.startActivity(end_intent);
            }
        }

        // Si no se quiere completar el quesito, se puede descomentar este código y el primero que
        // acierte una pregunta gana

        /*
        Intent end_intent = new Intent(mContext, WinScreenActivity.class);
        end_intent.putExtra(GANADOR, names[turno_del_juego]);
        mContext.startActivity(end_intent);
        */
    }

    /**
     * Actualiza el los estados de los jugadores
     */
    static void update_player_state()
    {
        if(turno_del_juego == 0)
        {
            estado_player_1.setText("JUGANDO");
            estado_player_1.setTextColor(Color.GREEN);
            estado_player_2.setText((String)"ESPERANDO");
            estado_player_2.setTextColor(Color.RED);
        }
        else
        {
            estado_player_1.setText((String)"ESPERANDO");
            estado_player_1.setTextColor(Color.RED);
            estado_player_2.setText((String)"JUGANDO");
            estado_player_2.setTextColor(Color.GREEN);
        }
    }

    /**
     * Inicializa los nombres de los jugadores y sus estados
     */
    private void preparar_jugadores()
    {
        TextView name_jugador_1 = findViewById(R.id.NombrePlayer1);
        TextView name_jugador_2 = findViewById(R.id.NombrePlayer2);

        name_jugador_1.setText(names[0]);
        name_jugador_2.setText(names[1]);

        estado_player_1 = findViewById(R.id.StatePlayer1);
        estado_player_2 = findViewById(R.id.StatePlayer2);

    }

    /**
     * Captura el intent e inicializa names[]
     */
    private void getMyIntent()
    {
        intent = getIntent();

        names[0] = intent.getStringExtra(AskNamesActivity.PLAYER_1_NAME);
        names[1] = intent.getStringExtra(AskNamesActivity.PLAYER_2_NAME);
    }

    @Override
    public void finish() {
        super.finish();
        System.exit(0);
    }
}
