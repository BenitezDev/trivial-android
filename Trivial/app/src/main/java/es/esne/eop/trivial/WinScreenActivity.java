package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Activity que felicita al jugador que ha ganado y la posibilidad de volver a jugar
 *
 * @author Alejandro Benítez en Enero 2019
 */
public class WinScreenActivity extends Activity
{

    // Texto en el cual se escribirá el ganador
    TextView texto_final;

    // Los dos únicos botones de la escena, volver a jugar y salir
    Button boton_salir, boton_volver_a_jugar;

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win_screen);

        intent = getIntent();



        boton_salir          = findViewById(R.id.salir       );
        boton_volver_a_jugar = findViewById(R.id.volverajugar);
        texto_final          = findViewById(R.id.texto_final );


        String winner = intent.getStringExtra(GameActivity.GANADOR);

        // En el activity_win_screen.xml esta puesto que escriba tod0 en mayúsculas
        texto_final.setText("enhorabuena "+winner+"! \n has ganado !" );


        // Funcionalidad del botón de volver a jugar
        boton_volver_a_jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent play_again = new Intent(getBaseContext(), AskNamesActivity.class);
                startActivity(play_again);
            }
        });

        // Funcionalidad del botón de salir
        boton_salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });


    }
}
