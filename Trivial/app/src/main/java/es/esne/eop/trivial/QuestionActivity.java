package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.esne.eop.trivial.questions.QuestionDB;
import es.esne.eop.trivial.questions.QuestionModel;

public class QuestionActivity extends Activity {

    // TextView donde se escribirá la pregunta
    private TextView pregunta;

    // Botones donde se escribirán las respuestas
    private Button respuesta_1, respuesta_2, respuesta_3;

    Intent intent = getIntent();


    String indice_pregunta;

    QuestionModel pregunta_actual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);


        pregunta    = findViewById(R.id.pregunta  );

        respuesta_1 = findViewById(R.id.respuesta1);
        respuesta_2 = findViewById(R.id.respuesta2);
        respuesta_3 = findViewById(R.id.respuesta3);

        indice_pregunta = GameActivity.indice_de_pregunta;



        generar_pregunta();


        // Funcionalidad del primer botón de la primera pregunta
        respuesta_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pregunta_actual.getCorrect() == 1)
                {
                    Toast.makeText(QuestionActivity.this, "HAS ACERTADO", Toast.LENGTH_SHORT).show();
                    GameActivity.acertar_pregunta();
                    finish();
                }
                else
                {
                    Toast.makeText(QuestionActivity.this, "HAS FALLADO", Toast.LENGTH_SHORT).show();
                    GameActivity.fallar_pregunta();
                    finish();

                }
            }
        });

        // Funcionalidad del segundo botón de la segunda pregunta
        respuesta_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pregunta_actual.getCorrect() == 2)
                {
                    Toast.makeText(QuestionActivity.this, "HAS ACERTADO", Toast.LENGTH_SHORT).show();
                    GameActivity.acertar_pregunta();
                    finish();
                }
                else
                {
                    Toast.makeText(QuestionActivity.this, "HAS FALLADO", Toast.LENGTH_SHORT).show();
                    GameActivity.fallar_pregunta();
                    finish();
                }
            }
        });

        // Funcionalidad del tercer botón de la tercera pregunta
        respuesta_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pregunta_actual.getCorrect() == 3)
                {
                    Toast.makeText(QuestionActivity.this, "HAS ACERTADO", Toast.LENGTH_SHORT).show();
                    GameActivity.acertar_pregunta();
                    finish();
                }
                else
                {
                    Toast.makeText(QuestionActivity.this, "HAS FALLADO", Toast.LENGTH_SHORT).show();
                    GameActivity.fallar_pregunta();
                    finish();
                }
            }
        });


    }


    /**
     * Genera una pregunta y la establece en pantalla
     */
   private void generar_pregunta ()
   {

       QuestionModel.Type tipo_pregunta;

        switch (indice_pregunta)
        {
            case "0":
                tipo_pregunta = QuestionModel.Type.BLUE;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);
                break;

            case "1":
                tipo_pregunta = QuestionModel.Type.PINK;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);
                break;

            case "2":
                tipo_pregunta = QuestionModel.Type.YELLOW;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);
                break;

            case "3":
                tipo_pregunta = QuestionModel.Type.BROWN;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);
                break;

            case "4":
                tipo_pregunta = QuestionModel.Type.GREEN;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);

                break;

            case "5":
                tipo_pregunta = QuestionModel.Type.ORANGE;
                pregunta_actual =  QuestionDB.getRandomQuestion(tipo_pregunta);
                break;
        }

       actualizar_textos();

   }

    /**
     * Cambia los textos de la pregunta y las opciones
     */
   private void actualizar_textos()
   {
       pregunta.setText(pregunta_actual.getStatement());
       respuesta_1.setText(pregunta_actual.getAnswer1());
       respuesta_2.setText(pregunta_actual.getAnswer2());
       respuesta_3.setText(pregunta_actual.getAnswer3());
   }
}
